﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPonCSharp
{
    internal interface Интерфейс 
    {
        string state { get; set; } 
        void StateAction(string state);

    }
    internal class Releaser: Интерфейс
    {
        public string state
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }
        public void StateAction(string value)
        {
            state = value;
        }
    }
}
