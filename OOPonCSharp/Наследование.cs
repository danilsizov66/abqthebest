﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPonCSharp
{
    internal class Наследование
    {
        public bool НаследуемоеПоле;
        public void Наследуемый_метод(string value) => Console.WriteLine(value);
    }
    internal class Наследник: Наследование
    {
        public bool ЕщёОдноПоле;
        public void Print(bool value) => Console.WriteLine(value);
    }
    internal class Main
    {
        Наследник Дэнчик = new Наследник();
        void main()
        {
            Дэнчик.Наследуемый_метод("Используем метод, который унаследовали!");
            Дэнчик.Print(Дэнчик.НаследуемоеПоле); //Передали значение унаследованного поля в метод наследника!
        }
    }
}
