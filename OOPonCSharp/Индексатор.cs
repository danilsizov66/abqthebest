﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPonCSharp
{
    class Индексатор
    {
        public string SomeValue { get; }
        public Индексатор(string value)
        {
            SomeValue = value;
        }
    }
    class SomeClass
    {
        Индексатор[] индексатор;
        public SomeClass (Индексатор[] value)
        {
            индексатор = value;
        }
        // индексатор
        public Индексатор this[int index]
        {
            get => индексатор[index];
            set => индексатор[index] = value;
        }
    }
}
