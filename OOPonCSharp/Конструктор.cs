﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPonCSharp
{
    public class someClass
    {
        string state;
        string state2;

        // Состояние State задаётся передаваемым
        // при инициализации конструктора аргументом value,
        // при этом передача этого аргумента является обязательной
        public someClass(string value)     
        {
            state = value;
            state2 = "Состояние state2 задаётся заранее" +
                "прописанным в конструкторе аргументом";
        }
        public someClass()
        {
            state = "Конструткоров может быть несколько, но их сигнатура";
            state2 = "должна отличаться!";
        }
    }
}
