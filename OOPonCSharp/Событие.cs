﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPonCSharp
{
    internal class Событие
    {
        public delegate void SomeDelegate(int value);
        //Делегат, имеющий чёткую сигнатуру
        public event SomeDelegate SomeEvent;
        //Событие будет иметь такую же сигнатуру, как и у делегата
        void Main()
        {
            Событие событие = new Событие();  
            событие.SomeEvent += MethodForEvent1; //подписали метод к этому событию
            событие.SomeEvent(10);                //Будет выведено: 10
            событие.SomeEvent -= MethodForEvent1; //отпписали метод от события
            событие.SomeEvent += MethodForEvent2; //подписали новый метод под событие
            событие.SomeEvent(10);                //Будет выведено: 20
        }
        public static void MethodForEvent1(int value)
        {
            Console.WriteLine(value);
        }
        public static void MethodForEvent2(int value)
        {
            Console.WriteLine(value + 10);
        }
    }
}