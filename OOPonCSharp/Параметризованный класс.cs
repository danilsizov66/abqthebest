﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPonCSharp
{
    class Параметризованный_класс<type> 
    {
        type SomeValue;
        public void PrintType()
        {
            Console.WriteLine(SomeValue.GetType());
        }
    }
    class MMain
    {
        public void main()
        {
            Параметризованный_класс<int> example1 = new Параметризованный_класс<int>();
            Параметризованный_класс<bool> example2 = new Параметризованный_класс<bool>();
            example1.PrintType(); //будет выведено int
            example2.PrintType(); //будет выведено bool
        } 
    }
}
