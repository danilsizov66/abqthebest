﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPonCSharp
{
    internal class Свойство
    {
        protected string state;
        public string state2 = "Пусть эта строка будет задана изначально";
        protected string stateGETSET //свойство для экземпляра класса Свойство
        {
            set //блок set, отвечает за установку значений свойства
            {
                state = "блок set, отвечает за установку значений свойства";
            }
            get //блок get, отвечает за получение значений свойства
            { 
                return state2; 
            }               
        }
    }
}
