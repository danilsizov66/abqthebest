﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OOPonCSharp;

namespace app
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Все методы для взаимодествия со списком можно посмотреть
            //зажав Ctrl и кликнув по названию класса List

            //Инициализируем список с типом данных int
            List<int> listochek = new List<int>();

            //Вызываем функции для работы со списком
            ListMethods.SetRandom5(listochek);

            //будут выведены все элементы списка (5)
            ListMethods.Display(listochek);

            //удаляем элемент с индексом 2
            //(индексация списков начинается от 0)
            listochek.RemoveAt(2);
            
            ListMethods.Display(listochek);

            //все элементы списка будут равны NULL!
            listochek.Clear();
            ListMethods.Display(listochek);
            
            Console.ReadLine();
        }
    }
}
