﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPonCSharp
{
    public delegate void TestNoParams(); //Делегат, сигнатура которого указывает,
                                         //что делегируемый метод не должен ничего не возвращает
                                         //и ничего не принимает

    public delegate string TestWithParams(string value); //Делегат, сигнатура которого указывает,
                                                         //что делегируемый метод должен возвращать
                                                         //и принимать (string, string value)
    internal class Делегат
    {
        static void Main()
        {
            TestNoParams noParams = ShowNoParams; //создаём экземпляр делегата и
                                                  //добавляем ему метод ShowNoParams

            noParams += MultiCast;                //дополнительно добавляем в делегат
                                                  //метод MultiCast

            TestWithParams withParams = ShowWithParams; //Создаём экземпляр делегата с
                                                        //другим добавленным методом

            withParams("Hello, Moto!");  //используем делегаты
            noParams();
            Console.ReadLine();
        }
        private static void MultiCast()
        {
            Console.WriteLine("thxfrwtchng");
        }
        private static string ShowWithParams(string value)
        {
            Console.WriteLine("Метод с параметрoм, который возвращает string");
            return value;
        }
        private static void ShowNoParams()
        {
            Console.WriteLine("Метод без параметров, который ничего не возвращает");
        }
    }
}
