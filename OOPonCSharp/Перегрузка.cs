﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPonCSharp
{
    public class Перегрузка
    {
        public string ПерегрузкаМетод()
        {
            return "Первая реализация метода, которая выводит эту строку и не имеет" +
                "входных параметров";
        }
        public string ПерегрузкаМетод(string value)
        {
            return value; //вторая реализация, параметр которой является строковой переменной,
                          //возвращаемой в ходе выполнения метода
        }
        public void ПерегрузкаМетод(int value)
        {
            for (int i = 0; i < value; i++)
            {
                Console.WriteLine("Этот метод принимает в качестве параметра число, которое" +
                "будет определять количество итераций в цикле вывода этой строки");
            }               
        }
    }
}
