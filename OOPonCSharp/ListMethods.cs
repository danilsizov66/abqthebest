﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPonCSharp
{
    //класс с моими методами
    public class ListMethods
    {
        public static void SetRandom5(List<int> list)
        {
            Random random = new Random();
            for (int i = 0; i < 5; i++)
            {
                //Добавление случайного элемента 
                list.Add(random.Next(10, 20));
            }
        }
        public static void Clear(List<int> list)
        {
            //Очистка списка
            list.Clear();
        }
        public static void Display(List<int> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                //получение элемента списка по индексу
                Console.WriteLine($"{list[i].ToString()}");
            }
            Console.WriteLine("\n");
        }
    }
}
