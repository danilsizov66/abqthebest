﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPonCSharp
{
    internal class Инкапсуляция
    {
        private bool InPapaClassChange = true; //модификатор доступности private - значение поля
                                               //изменяется только в родительском классе
        public bool InAllClassChange;          //модификатор доступности public - значение поля
                                               //можно менятьиз любого класса

        private void PrivateMethod() => Console.WriteLine(InPapaClassChange);  
        public void PublicMethod() => Console.WriteLine(InAllClassChange = false);
    }

    internal class ТестИнкапсуляции
    {
        void maine()
        {
            Инкапсуляция тест1 = new Инкапсуляция();

            тест1.InAllClassChange = true; //Открытое поле можно спокойно изменить
            тест1.PublicMethod();          //Открытый метод можно спокойно использовать

            //тест1.InPapaClassChange = false; //Закрытое поле и метод невозможно использовать
            //тест1.PrivateMethod();           //или изменять в других класах
        }
    }
}
