﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPonCSharp
{
    internal struct Структура
    {
        int SomeValue;

        public Структура(int SomeValue)
        {
            this.SomeValue = SomeValue;
        }

        public int SomeMethod()
        {
            return SomeValue;
        }
    }
}
